seguir = True
suma_impares = 0

while seguir:
    try:
        print("Dame un numero entero no negativo: ")
        numero1 = int(input())
        print("Dame otro:")
        numero2 = int(input())
        if numero1  >= 0 <= numero2:
            seguir = False
        else:
            print("Por favor, ingresa otro numero no negativo:")
    except:
        print("Introduce un numero entero")

for numero in range(numero1, numero2 + 1):
    if numero % 2 != 0:
        suma_impares += numero

print("La suma de los numeros impares entre", numero1, "y", numero2, "es:", suma_impares)